package com.arsa.age_in_minutes

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.arsa.age_in_minutes.databinding.ActivityMainBinding
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnDatePicker.setOnClickListener {view ->
            clickDatePicker(view)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun clickDatePicker(view: View) {

        val myCalendar:Calendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val minute = myCalendar.get(Calendar.MINUTE)
        val hourOfDay = myCalendar.get(Calendar.HOUR_OF_DAY)

        DatePickerDialog (this, DatePickerDialog.OnDateSetListener { view, selectedYear, selectedMonth, selectedDayOfMonth ->
            var selectedDate = "$selectedDayOfMonth/${selectedMonth+1}/$selectedYear"


            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            selectedDate += " $hourOfDay:$minute"

            binding.tvSelectedDate.setText(selectedDate)
            },minute,
            hourOfDay, false).show()


        }
            ,year,
             month,
             day).show()
        }

    fun calculateAge(view: View) {
        val today = Date()
        val dobs:String = binding.tvSelectedDate.text.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH)
        val dob:Date? = sdf.parse(dobs)

        val days:Long = (today.time - dob!!.time)/86400000
        val hours:Long = (today.time - dob!!.time)%86400000/3600000
        val minutes:Long = (today.time - dob!!.time)%86400000%3600000/60000
        val sec:Long = (today.time - dob!!.time)%86400000%3600000%60000/1000
        binding.tvSelectedDateInMinute.setText("Days = $days\nHours = $hours\nMinutes = $minutes\nSeconds = $sec")


    }


}
